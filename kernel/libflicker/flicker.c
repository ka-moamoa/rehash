// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <msp430.h>
#include "digipot.h"


void initPort1(){
    P4OUT &= ~BIT1;
    P4DIR |= BIT1;
}

/************* Turn on flicker's port1 **************/
void onPort1(){
    P4OUT |= BIT1;
}

/************* Turn off flicker's port1 *************/
void offPort1(){
    P4OUT &= ~BIT1;
}

/********** Set digipot of flicker's port1 **********/
void configPort1(){
}

void initPort2(){
    P5OUT &= ~BIT0;
    P5DIR |= BIT0;
}

/************* Turn on flicker's port2 **************/
void onPort2(){
    P5OUT |= BIT0;
}

/************* Turn off flicker's port2 *************/
void offPort2(){
    P5OUT &= ~BIT0;
}

/********** Set digipot of flicker's port2 **********/
void configPort2(){
    P2DigipotSpiConfig();
    P2SetDigipot();
}

void initPort3(){
    PJOUT &= ~BIT1;
    PJDIR |= BIT1;
}

/************* Turn on flicker's port3 **************/
void onPort3(){
    PJOUT |= BIT1;
}

/************* Turn off flicker's port3 *************/
void offPort3(){
    PJOUT &= ~BIT1;
}

/********** Set digipot of flicker's port3 **********/
void configPort3(){
    P3DigipotSpiConfig();
    P3SetDigipot();
}


