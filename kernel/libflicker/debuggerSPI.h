// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef DEBUGGERSPI_H_
#define DEBUGGERSPI_H_

#include "macros.h"
#include <stdint.h>
#include <msp430.h>


#define SPI_CS_DBG_GPIO_BIT__             3
#define SPI_CONFIG_CS_DBG_PIN_AS_OUTPUT()   st( P2DIR |=  BV(SPI_CS_DBG_GPIO_BIT__); )
#define SPI_DRIVE_CS_DBG_HIGH()             st( P2OUT |=  BV(SPI_CS_DBG_GPIO_BIT__); ) /* atomic operation */
#define SPI_DRIVE_CS_DBG_LOW()              st( P2OUT &= ~BV(SPI_CS_DBG_GPIO_BIT__); ) /* atomic operation */
#define SPI_CS_DBG_IS_HIGH()                 (  P2OUT &   BV(SPI_CS_DBG_GPIO_BIT__) )

/* SCLK_CTRL Pin Configuration */
#define SPI_SCLK_DBG_GPIO_BIT__            0
#define SPI_CONFIG_SCLK_DBG_PIN_AS_OUTPUT()  st( P3DIR |=  BV(SPI_SCLK_DBG_GPIO_BIT__); )
#define SPI_DRIVE_SCLK_DBG_HIGH()            st( P3OUT |=  BV(SPI_SCLK_DBG_GPIO_BIT__); )
#define SPI_DRIVE_SCLK_DBG_LOW()             st( P3OUT &= ~BV(SPI_SCLK_DBG_GPIO_BIT__); )

/* SI_CTRL Pin Configuration */
#define SPI_SI_DBG_GPIO_BIT__              1
#define SPI_CONFIG_SI_DBG_PIN_AS_OUTPUT()    st( P3DIR |=  BV(SPI_SI_DBG_GPIO_BIT__); )
#define SPI_DRIVE_SI_DBG_HIGH()              st( P3OUT |=  BV(SPI_SI_DBG_GPIO_BIT__); )
#define SPI_DRIVE_SI_DBG_LOW()               st( P3OUT &= ~BV(SPI_SI_DBG_GPIO_BIT__); )

/* SO_CTRL Pin Configuration */
#define SPI_SO_DBG_GPIO_BIT__              2
#define SPI_CONFIG_SO_DBG_PIN_AS_INPUT()     st( P3DIR &= ~BV(SPI_SO_DBG_GPIO_BIT__);)
#define SPI_SO_DBG_IS_HIGH()                 ( P3IN & BV(SPI_SO_DBG_GPIO_BIT__) )

/* SPI_CTRL Port Configuration */
#define SPI_DBG_CONFIG_PORT()                   st( P3SEL1 &= ~(BV(SPI_SI_DBG_GPIO_BIT__)   |  BV(SPI_SO_DBG_GPIO_BIT__)| BV(SPI_SCLK_DBG_GPIO_BIT__)); \
                                              P3SEL0 |= BV(SPI_SI_DBG_GPIO_BIT__) | BV(SPI_SO_DBG_GPIO_BIT__) | BV(SPI_SCLK_DBG_GPIO_BIT__);)

#define SPI_DBG_INIT() \
st ( \
  SPI_DBG_CONFIG_PORT();                       \
  UCB1CTLW0 = UCSWRST;                 \
  UCB1CTLW0 |= UCCKPH | UCMSB | UCMST | UCSYNC;   \
  UCB1CTLW0 |= UCSSEL__SMCLK;                 \
  UCB1BRW  = 0x00;                                 \
  UCB1CTLW0 &= ~UCSWRST;                         \
)

#define SPI_DBG_WRITE_BYTE(x)                st( UCB1IFG &= ~UCRXIFG;  UCB1TXBUF = x; )
#define SPI_DBG_READ_BYTE()                  UCB1RXBUF
#define SPI_DBG_WAIT_DONE()                  while(!(UCB1IFG & UCRXIFG));

uint8_t SpiDbgTransfer(uint8_t value);
void DbgSpiconfig();
void SpiDbgWrite(uint8_t value);


#endif /* DEBUGGERSPI_H_ */
