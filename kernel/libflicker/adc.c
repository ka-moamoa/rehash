// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include "adc.h"

void adcConfig()
{
    // Configure ADC12
    ADC12CTL0 &= ~ADC12ENC;                                 // Disable ADC12
    ADC12CTL0 |= ADC12SHT0_2 | ADC12ON;                               // Sampling time, S&H=16, ADC12 on
    ADC12CTL1 |= ADC12SHP;                                   // Use sampling timer
    ADC12CTL2 |= ADC12RES_2;                                // 12-bit conversion results
    ADC12MCTL0 |= ADC12VRSEL_0| ADC12INCH_9;                              // A2 ADC input select; Vref=AVCC

    P9SEL1 |= BIT1;                                         // Configure P9.1 for ADC
    P9SEL0 |= BIT1;
}

unsigned int adcSample()
{
    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 |= ADC12ON;                   // Turn on ADC
    ADC12CTL0 |= ADC12ENC | ADC12SC;        // Enable conversions + Start sampling/conversion

    while (!(ADC12IFGR0 & BIT0));
    unsigned int adcVal = ADC12MEM0;

    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 &= ~ADC12ON;                  // Turn off ADC

    ADC12CTL0 = 0;                          // Disable ADC12
    ADC12CTL0 = 0;                          // Set sample time
    ADC12CTL1 = 0;                          // Enable sample timer
    ADC12CTL3 = 0;                          // Enable internal temperature sensor
    ADC12MCTL0 = 0;                         // ADC input ch A30 => temp sense

    return adcVal;
}

void tempConfig()
{
    // Initialize ADC12_A
    ADC12CTL0 &= ~ADC12ENC;                 // Disable ADC12
    ADC12CTL0 |= ADC12SHT0_8;                // Set sample time
    ADC12CTL1 |= ADC12SHP;                   // Enable sample timer
    ADC12CTL3 |= ADC12TCMAP;                 // Enable internal temperature sensor
    ADC12MCTL0 |= ADC12VRSEL_1| ADC12INCH_30; // ADC input ch A30 => temp sense
}

float tempDegC()
{
    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 |= ADC12ON;                   // Turn on ADC
    ADC12CTL0 |= ADC12ENC | ADC12SC;        // Start sampling/conversion

    while (!(ADC12IFGR0 & BIT0));
    unsigned int adcVal = ADC12MEM0;
    float temperatureDegC = (float)(((float)adcVal - CALADC12_12V_30C) * (85 - 30)) / (CALADC12_12V_85C - CALADC12_12V_30C) + 30.0f;


    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 &= ~ADC12ON;                  // Turn off ADC

    ADC12CTL0 = 0;                          // Disable ADC12
    ADC12CTL0 = 0;                          // Set sample time
    ADC12CTL1 = 0;                          // Enable sample timer
    ADC12CTL3 = 0;                          // Enable internal temperature sensor
    ADC12MCTL0 = 0;                         // ADC input ch A30 => temp sense

    return temperatureDegC;
}

void Timekeeper_config()
{
    ADC12CTL0 &= ~ADC12ENC;                                 // Disable ADC12
    ADC12CTL0 = ADC12SHT0_2 | ADC12ON;                      // Sampling time, S&H=16, ADC12 on
    ADC12CTL1 = ADC12SHP;                                   // Use sampling timer
    ADC12CTL2 |= ADC12RES_2;                                // 12-bit conversion results
    ADC12MCTL0 |= ADC12INCH_3 | ADC12VRSEL_0;  // A0 ADC input select; Vref=AVCC

    P1SEL1 |= BIT3;                             // Configure P1.3 for ADC
    P1SEL0 |= BIT3;
}

unsigned int Timekeeper_sample()
{
    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 |= ADC12ON;                   // Turn on ADC
    ADC12CTL0 |= ADC12ENC;                  // Enable conversions

    ADC12CTL0 |= ADC12SC;                   // Start sampling/conversion
    while (!(ADC12IFGR0 & BIT0));
    unsigned int adcVal = ADC12MEM0;

    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions
    ADC12CTL0 &= ~ADC12ON;                  // Turn off ADC
    ADC12CTL0 &= ~ADC12ENC;                 // Disable conversions

    ADC12CTL0 = 0;                          // Disable ADC12
    ADC12CTL0 = 0;                          // Set sample time
    ADC12CTL1 = 0;                          // Enable sample timer
    ADC12CTL3 = 0;                          // Enable internal temperature sensor
    ADC12MCTL0 = 0;                         // ADC input ch A30 => temp sense

    return adcVal;
}
