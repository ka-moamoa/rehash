// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include "debuggerSPI.h"

uint8_t SpiDbgTransfer(uint8_t value)
{
    uint8_t statusByte;
    /* send the command strobe, wait for SPI access to complete */
    SPI_DBG_WRITE_BYTE(value);
    SPI_DBG_WAIT_DONE();

    /* read the radio status uint8_t returned by the command strobe */
    statusByte = SPI_DBG_READ_BYTE();
    //debug_stats[debug_index] = statusByte;
    //debug_index++;
    return statusByte;
}

void DbgSpiconfig()
{

    SPI_CONFIG_CS_DBG_PIN_AS_OUTPUT();
    SPI_CONFIG_SCLK_DBG_PIN_AS_OUTPUT();
    SPI_CONFIG_SI_DBG_PIN_AS_OUTPUT();
    SPI_CONFIG_SO_DBG_PIN_AS_INPUT();

    SPI_DRIVE_CS_DBG_HIGH();

    SPI_DBG_INIT();
}

void SpiDbgWrite(uint8_t value)
{
    DbgSpiconfig();

    SPI_DRIVE_CS_DBG_LOW();
    while (SPI_SO_DBG_IS_HIGH());
    SpiDbgTransfer(value);
    SPI_DRIVE_CS_DBG_HIGH();
//    __delay_cycles(2000);
}
