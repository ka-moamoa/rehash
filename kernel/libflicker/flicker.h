// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef NEW_LIBS_FLICKER_H_
#define NEW_LIBS_FLICKER_H_

void initPort2();
void onPort2();
void offPort2();
void configPort2();

void initPort3();
void onPort3();
void offPort3();
void configPort3();

void initPort1();
void onPort1();
void offPort1();
void configPort1();


#endif /* NEW_LIBS_FLICKER_H_ */
