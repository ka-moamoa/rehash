// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#ifndef DIGIPOT_H_
#define DIGIPOT_H_

#include <msp430.h>
#include "macros.h"
#include <stdint.h>

#define SPI_CS_P2_CTRL_GPIO_BIT__             0
#define SPI_CONFIG_CS_P2_CTRL_PIN_AS_OUTPUT()   st( P1DIR |=  BV(SPI_CS_P2_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_CS_P2_CTRL_HIGH()             st( P1OUT |=  BV(SPI_CS_P2_CTRL_GPIO_BIT__); ) /* atomic operation */
#define SPI_DRIVE_CS_P2_CTRL_LOW()              st( P1OUT &= ~BV(SPI_CS_P2_CTRL_GPIO_BIT__); ) /* atomic operation */
#define SPI_CS_P2_CTRL_IS_HIGH()                 (  P1OUT &   BV(SPI_CS_P2_CTRL_GPIO_BIT__) )

#define SPI_CS_P3_CTRL_GPIO_BIT__             7
#define SPI_CONFIG_CS_P3_CTRL_PIN_AS_OUTPUT()   st( P3DIR |=  BV(SPI_CS_P3_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_CS_P3_CTRL_HIGH()             st( P3OUT |=  BV(SPI_CS_P3_CTRL_GPIO_BIT__); ) /* atomic operation */
#define SPI_DRIVE_CS_P3_CTRL_LOW()              st( P3OUT &= ~BV(SPI_CS_P3_CTRL_GPIO_BIT__); ) /* atomic operation */
#define SPI_CS_P3_CTRL_IS_HIGH()                 (  P3OUT &   BV(SPI_CS_P3_CTRL_GPIO_BIT__) )

/* SCLK_CTRL Pin Configuration */
#define SPI_SCLK_CTRL_GPIO_BIT__            0
#define SPI_CONFIG_SCLK_CTRL_PIN_AS_OUTPUT()  st( P3DIR |=  BV(SPI_SCLK_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_SCLK_CTRL_HIGH()            st( P3OUT |=  BV(SPI_SCLK_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_SCLK_CTRL_LOW()             st( P3OUT &= ~BV(SPI_SCLK_CTRL_GPIO_BIT__); )

/* SI_CTRL Pin Configuration */
#define SPI_SI_CTRL_GPIO_BIT__              1
#define SPI_CONFIG_SI_CTRL_PIN_AS_OUTPUT()    st( P3DIR |=  BV(SPI_SI_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_SI_CTRL_HIGH()              st( P3OUT |=  BV(SPI_SI_CTRL_GPIO_BIT__); )
#define SPI_DRIVE_SI_CTRL_LOW()               st( P3OUT &= ~BV(SPI_SI_CTRL_GPIO_BIT__); )

/* SO_CTRL Pin Configuration */
#define SPI_SO_CTRL_GPIO_BIT__              2
#define SPI_CONFIG_SO_CTRL_PIN_AS_INPUT()     st( P3DIR &= ~BV(SPI_SO_CTRL_GPIO_BIT__); \
                                                P3REN |= BV(SPI_SO_CTRL_GPIO_BIT__); \
                                                P3OUT |= BV(SPI_SO_CTRL_GPIO_BIT__);)

#define SPI_SO_CTRL_IS_HIGH()                 ( P3IN & BV(SPI_SO_CTRL_GPIO_BIT__) )

/* SPI_CTRL Port Configuration */
#define SPI_CTRL_CONFIG_PORT()                   st( P3SEL1 &= ~(BV(SPI_SI_CTRL_GPIO_BIT__)   |  BV(SPI_SO_CTRL_GPIO_BIT__)| BV(SPI_SCLK_CTRL_GPIO_BIT__)); \
                                              P3SEL0 |= BV(SPI_SI_CTRL_GPIO_BIT__) | BV(SPI_SO_CTRL_GPIO_BIT__) | BV(SPI_SCLK_CTRL_GPIO_BIT__);)

#define SPI_CTRL_INIT() \
st ( \
  SPI_CTRL_CONFIG_PORT();                       \
  UCB1CTLW0 = UCSWRST;                 \
  UCB1CTLW0 |= UCCKPH | UCMSB | UCMST | UCSYNC;   \
  UCB1CTLW0 |= UCSSEL_1;                 \
  UCB1BRW  = 0x02;                                 \
  UCB1CTLW0 &= ~UCSWRST;                         \
)

#define SPI_CTRL_WRITE_BYTE(x)                st( UCB1TXBUF = x; )
#define SPI_CTRL_READ_BYTE()                  UCB1RXBUF
#define SPI_CTRL_WAIT_DONE()                  while(!(UCB1IFG & UCRXIFG));

uint8_t SpiCtrlTransfer(uint8_t value);
void P2DigipotSpiConfig();
void P2SetDigipot();
void P3DigipotSpiConfig();
void P3SetDigipot();

#endif /* DIGIPOT_H_ */
