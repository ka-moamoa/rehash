// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "digipot.h"

uint8_t SpiCtrlTransfer(uint8_t value)
{
    uint8_t statusByte;
    /* send the command strobe, wait for SPI access to complete */
    SPI_CTRL_WRITE_BYTE(value);
    SPI_CTRL_WAIT_DONE();

    /* read the radio status uint8_t returned by the command strobe */
    statusByte = SPI_CTRL_READ_BYTE();
    //debug_stats[debug_index] = statusByte;
    //debug_index++;
    return statusByte;
}

void P2DigipotSpiConfig()
{
    SPI_CONFIG_CS_P2_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SCLK_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SI_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SO_CTRL_PIN_AS_INPUT();

    SPI_DRIVE_CS_P2_CTRL_HIGH();

    SPI_CTRL_INIT();
}
void P2SetDigipot()
{
//    SPI_DRIVE_CS_P2_CTRL_LOW();
//    while (SPI_SO_IS_HIGH());
//    SpiCtrlTransfer(0b01100000);
//    __delay_cycles(100);
//    SpiCtrlTransfer(0b01000000);
//    __delay_cycles(100);
//    SPI_DRIVE_CS_P2_CTRL_HIGH();

    SPI_DRIVE_CS_P2_CTRL_LOW();
//    while (SPI_SO_CTRL_IS_HIGH());
    SpiCtrlTransfer(0b11000000);
    __delay_cycles(100);
    SpiCtrlTransfer(0b00000000);
    __delay_cycles(100);
    SPI_DRIVE_CS_P2_CTRL_HIGH();

    SPI_DRIVE_CS_P2_CTRL_LOW();
//    while (SPI_SO_CTRL_IS_HIGH());
    SpiCtrlTransfer(0b11000001);
    __delay_cycles(100);
    SpiCtrlTransfer(0b00000000);
    __delay_cycles(100);
    SPI_DRIVE_CS_P2_CTRL_HIGH();

}

void P3DigipotSpiConfig()
{
    SPI_CONFIG_CS_P3_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SCLK_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SI_CTRL_PIN_AS_OUTPUT();
    SPI_CONFIG_SO_CTRL_PIN_AS_INPUT();

    SPI_DRIVE_CS_P3_CTRL_HIGH();

    SPI_CTRL_INIT();
}
void P3SetDigipot()
{
//    SPI_DRIVE_CS_P3_CTRL_LOW();
//    while (SPI_SO_IS_HIGH());
//    SpiCtrlTransfer(0b01100000);
//    __delay_cycles(100);
//    SpiCtrlTransfer(0b01000000);
//    __delay_cycles(100);
//    SPI_DRIVE_CS_P3_CTRL_HIGH();

    SPI_DRIVE_CS_P3_CTRL_LOW();
//    while (SPI_SO_CTRL_IS_HIGH());
    SpiCtrlTransfer(0b11000000);
    __delay_cycles(100);
    SpiCtrlTransfer(0b10101010);
    __delay_cycles(100);
    SPI_DRIVE_CS_P3_CTRL_HIGH();

    SPI_DRIVE_CS_P3_CTRL_LOW();
//    while (SPI_SO_CTRL_IS_HIGH());
    SpiCtrlTransfer(0b11000001);
    __delay_cycles(100);
    SpiCtrlTransfer(0b10101010);
    __delay_cycles(100);
    SPI_DRIVE_CS_P3_CTRL_HIGH();

}
