// This file is part of InK.
// 
// author = "Kasım Sinan Yıldırım " 
// maintainer = "Kasım Sinan Yıldırım "
// email = "sinanyil81 [at] gmail.com" 
//  
// copyright = "Copyright 2018 Delft University of Technology" 
// license = "LGPL" 
// version = "3.0" 
// status = "Production"
//
// 
// InK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * This file has been modified for REHASH
 * Please visit InK repository to access the original code
 * https://github.com/TUDSSL/InK
 */

#ifndef REHASH_H_
#define REHASH_H_

#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#include "mcu/mcu.h"
#include "scheduler/scheduler.h"
#include "isr/isr.h"
#include "channel/channel.h"
#include "timer/timer.h"
#include "heuristics/heuristics.h"
#include "libflicker/debuggerSPI.h"
#include "libflicker/digipot.h"
#include "libflicker/adc.h"
#include "libflicker/flicker.h"

#define ADAPT
//#define DEBUG
//#define TIMEKEEPER

//#define __adaptive __nv

#define __knob(type, name, value) static __nv type _adaptive_global_ ## name = value
#define __WRITE_KNOB(name, value) _adaptive_global_ ## name = value
#define __READ_KNOB(name) _adaptive_global_ ## name

#define __TUNE_UP(var, num) __WRITE_KNOB(var, __READ_KNOB(var) - num)
#define __TUNE_DOWN(var, num) __WRITE_KNOB(var, __READ_KNOB(var) + num)

#define __ADAPT_UP void adaptUp()
#define __ADAPT_DOWN void adaptDown()


/*
* Module settings
*/

//#define WKUP_TIMER
//#define XPR_TIMER
//#define TIMERS_ON

#endif /* REHASH_H_ */
