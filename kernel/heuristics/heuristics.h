// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef HEURISTICS_H_
#define HEURISTICS_H_

#include "rehash.h"

#define DATA_POINTS_N 65
#define MILLI_INTERVAL 30.76923076923077
#define SHIFT_BITS 6

#define HISTORY 2
#define ON_RECORD_FIRST
//#define ON_RECORD_LAST
#define ON_TIME_UPPER_DELTA 0.0
#define ON_TIME_LOWER_DELTA 0.00005

//#define OFF_RECORD_FIRST
//#define OFF_RECORD_LAST
#define OFF_TIME_UPPER_DELTA 0.003
#define OFF_TIME_LOWER_DELTA 0.003

//#define TASK_RECORD_FIRST
#define TASK_RECORD_LAST
#define TASK_COUNT_UPPER_DELTA 0.15
#define TASK_COUNT_LOWER_DELTA 0.15

#define EXPIRATION_DEADLINE 2150 //milliseconds
#define DEADLINE_UPPER_DELTA 0.01
#define DEADLINE_LOWER_DELTA 0.01

#define POWER_FAILURE_UPPER_DELTA 0
#define POWER_FAILURE_LOWER_DELTA 0

static __nv uint16_t onTime[HISTORY+1] = {0};
static __nv uint16_t offTime[HISTORY+1] = {0xffff};
static __nv uint32_t taskCount[HISTORY+1] = {0};
static __nv uint32_t deadlineTime[HISTORY+1] = {0};
static __nv uint16_t powerFailureCount[HISTORY+1] = {0xffff};

static __nv uint16_t temp_on_time_count = 0;
static __nv uint16_t temp_off_time = 0;
static __nv uint32_t temp_task_count = 0;
static __nv uint16_t temp_power_failure_count = 0;

static __nv uint8_t task_count_lock = 1;
static __nv uint8_t on_time_lock = 1;
static __nv uint8_t off_time_lock = 1;
static __nv uint8_t deadline_lock = 1;
static __nv uint8_t power_failure_lock = 1;

static __nv uint8_t task_weight = 0;

typedef enum {NONE = 0, ON_TIME = 1, OFF_TIME = 2, TASK_COUNT = 3, DEADLINE = 4, POWER_FAILURE_COUNT = 5} signal_t;
static __nv signal_t adapt = NONE;

void checkAdaptation();
void selectAdaptationHeuristic(signal_t adapt);

void onHeuristicEnable();
void onHeuristicDisable();

void offHeuristicEnable();
void offHeuristicDisable();

void taskHeuristicEnable();
void taskHeuristicDisable();

void deadlineHeuristicEnable();
void deadlineHeuristicDisable();

void powerFailureHeuristicEnable();
void powerFailureHeuristicDisable();

void calcOffTime();
void setOffTime(uint16_t t);
uint16_t* getOffTime();

void calcOnTime();
void setOnTime(uint16_t t);
uint16_t* getOnTime();

void calcTaskCount();
void incTaskCount();
void setTaskWeight(uint8_t w);
void setTaskCount(uint32_t c);
uint32_t* getTaskCount();

void calcDeadlineTime();
void setDeadlineTime(uint32_t t);
uint32_t* getDeadlineTime();

void calcPowerFailureCount();
void incPowerFailureCount();
void setPowerFailureCount(uint16_t t);
uint16_t* getPowerFailureCount();

void sleepMode(uint16_t cycles);

#endif /* HEURISTICS_H_ */
