// This file is part of REHASH.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.


#include <msp430.h>
#include "libflicker/adc.h"
#include "heuristics.h"

static uint16_t calcAvg_16bit(uint16_t *t)
{
    float sum = 0;
    int i = 1;
    for (i = 1; i < HISTORY + 1; i++)
        sum += t[i];
    uint16_t avg = sum / ((float) HISTORY);
    return avg;
}

static uint32_t calcAvg_32bit(uint32_t *t)
{
    float sum = 0;
    int i = 1;
    for (i = 1; i < HISTORY + 1; i++)
        sum += t[i];
    uint32_t avg = sum / ((float) HISTORY);
    return avg;
}

extern void adaptDown();
extern void adaptUp();

void selectAdaptationHeuristic(signal_t adaptation_signal)
{
    adapt = adaptation_signal;
}

void checkAdaptation()
{
    uint16_t* on_Time = 0;
    uint16_t* off_Time = 0;
    uint32_t* task_Count = 0;
    uint32_t* deadline_Time = 0;
    uint16_t* power_Failure_Count = 0;

    unsigned i;

    switch (adapt)
    {
    case ON_TIME:
    {
        onHeuristicEnable();
        on_Time = getOnTime();
        uint16_t avgTime = calcAvg_16bit(on_Time);
        if (on_Time[0] < avgTime - (ON_TIME_LOWER_DELTA * avgTime))
        {
            adaptDown();
        }
        else if (on_Time[0] > avgTime + (ON_TIME_UPPER_DELTA * avgTime))
        {
            adaptUp();
        }

#ifdef DEBUG
        __port_toggle(4, 1);
        SpiDbgWrite((avgTime >> 8) & 0xFF);
        SpiDbgWrite(avgTime & 0xFF);
        for (i = 0; i < HISTORY + 1; i++)
        {
            SpiDbgWrite((on_Time[i] >> 8) & 0xFF);
            SpiDbgWrite(on_Time[i] & 0xFF);
        }
        __port_toggle(4, 1);
#endif
        break;
    }

    case OFF_TIME:
    {
        offHeuristicEnable();
        off_Time = getOffTime();
        uint16_t avgTime1 = calcAvg_16bit(off_Time);
        if (off_Time[0] > avgTime1 + (OFF_TIME_UPPER_DELTA * avgTime1))
        {
            adaptDown();
        }
        else if (off_Time[0] < avgTime1 - (OFF_TIME_LOWER_DELTA * avgTime1))
        {
            adaptUp();
        }

#ifdef DEBUG
        __port_toggle(4, 1);
        SpiDbgWrite((avgTime1 >> 8) & 0xFF);
        SpiDbgWrite(avgTime1 & 0xFF);
        for (i = 0; i < HISTORY + 1; i++)
        {
            SpiDbgWrite((off_Time[i] >> 8) & 0xFF);
            SpiDbgWrite(off_Time[i] & 0xFF);
        }
        __port_toggle(4, 1);
#endif
        break;
    }

    case TASK_COUNT:
    {
        taskHeuristicEnable();
        task_Count = getTaskCount();
        uint32_t avgCount = calcAvg_32bit(task_Count);
        if (task_Count[0] < avgCount - (TASK_COUNT_LOWER_DELTA * avgCount))
        {
            adaptDown();
        }
        else if (task_Count[0] > avgCount + (TASK_COUNT_UPPER_DELTA * avgCount))
        {
            adaptUp();
        }

#ifdef DEBUG
        __port_toggle(4, 1);
        SpiDbgWrite((avgCount >> 24) & 0xFF);
        SpiDbgWrite((avgCount >> 16) & 0xFF);
        SpiDbgWrite((avgCount >> 8) & 0xFF);
        SpiDbgWrite(avgCount & 0xFF);
        for (i = 0; i < HISTORY + 1; i++)
        {
            SpiDbgWrite((task_Count[i] >> 24) & 0xFF);
            SpiDbgWrite((task_Count[i] >> 16) & 0xFF);
            SpiDbgWrite((task_Count[i] >> 8) & 0xFF);
            SpiDbgWrite(task_Count[i] & 0xFF);
        }
        __port_toggle(4, 1);
#endif
        break;
    }
    case DEADLINE:
    {
        deadlineHeuristicEnable();
        deadline_Time = getDeadlineTime();
        uint32_t avgTime= calcAvg_32bit(deadline_Time);

        if (deadline_Time[0] > EXPIRATION_DEADLINE)
        {
            adaptDown();
        }
        else if (avgTime < EXPIRATION_DEADLINE - (EXPIRATION_DEADLINE * DEADLINE_UPPER_DELTA))
        {
            adaptUp();
        }

#ifdef DEBUG
        __port_toggle(4, 1);
        SpiDbgWrite((avgTime >> 24) & 0xFF);
        SpiDbgWrite((avgTime >> 16) & 0xFF);
        SpiDbgWrite((avgTime >> 8) & 0xFF);
        SpiDbgWrite(avgTime & 0xFF);
        for (i = 0; i < HISTORY + 1; i++)
        {
            SpiDbgWrite((deadline_Time[i] >> 24) & 0xFF);
            SpiDbgWrite((deadline_Time[i] >> 16) & 0xFF);
            SpiDbgWrite((deadline_Time[i] >> 8) & 0xFF);
            SpiDbgWrite(deadline_Time[i] & 0xFF);
        }
        __port_toggle(4, 1);
#endif
        break;
    }
    case POWER_FAILURE_COUNT:
        {
            powerFailureHeuristicEnable();
            power_Failure_Count = getPowerFailureCount();
            uint16_t avgFailureCount = calcAvg_16bit(power_Failure_Count);
            if (power_Failure_Count[0] > avgFailureCount + (POWER_FAILURE_UPPER_DELTA * avgFailureCount))
            {
                adaptDown();
            }
            else if (power_Failure_Count[0] < avgFailureCount - (POWER_FAILURE_LOWER_DELTA * avgFailureCount))
            {
                adaptUp();
            }

    #ifdef DEBUG
            __port_toggle(4, 1);
            SpiDbgWrite((avgFailureCount >> 8) & 0xFF);
            SpiDbgWrite(avgFailureCount & 0xFF);
            for (i = 0; i < HISTORY + 1; i++)
            {
                SpiDbgWrite((power_Failure_Count[i] >> 8) & 0xFF);
                SpiDbgWrite(power_Failure_Count[i] & 0xFF);
            }
            __port_toggle(4, 1);
    #endif
            break;
        }
    default:
    {
        break;
    }
    }
}

void onHeuristicEnable()
{
    TA0CCR0 = 60; //  this is changed to 60 from 120 for deadline heuristic
    TA0CTL = TASSEL__ACLK | MC__UP | ID_0; // ACLK (originally SMCLK with 30000, changed due to sleep mode problem), UP mode
    TA0CCTL0 |= CCIE; // TACCR0 interrupt enabled

    on_time_lock = 0;
    __bis_SR_register(GIE); // Enable general interrupt
}

void onHeuristicDisable()
{
    TA0CTL = MC__STOP;
    TA0CCTL0 &= ~CCIE;                          // TACCR0 interrupt disabled
    on_time_lock = 1;
}

void offHeuristicEnable()
{
    TA1CCR0 = 120;
    TA1CTL = TASSEL__ACLK | MC__UP | ID_0; // ACLK (originally SMCLK with 30000, changed due to sleep mode problem), UP mode
    TA1CCTL0 = CCIE; // TACCR0 interrupt enabled

    P2DIR |= BIT2; // making output
    P2OUT |= BIT2; // Charging pin on
    P2REN &= ~BIT2; // resistor disable
    uint16_t delay = offTime[0] / 500.0;
    int i = 0;
    do
    {
        __delay_cycles(100);
        i++;
    }
    while (i < delay);
    P2DIR &= ~BIT2;                             // making input
    P2REN |= BIT2;                             // resistor enable
    P2OUT &= ~BIT2;                             // pull down

    off_time_lock = 0;
    __bis_SR_register(GIE);                          // Enable general interrupt
}
void offHeuristicDisable()
{
    TA1CTL = MC__STOP;
    TA1CCTL0 &= ~CCIE;                           // TACCR0 interrupt enabled
    off_time_lock = 1;
}

void taskHeuristicEnable()
{
    task_count_lock = 0;
}

void taskHeuristicDisable()
{
    task_count_lock = 1;
}

void deadlineHeuristicEnable()
{
    onHeuristicEnable();
    offHeuristicEnable();
    deadline_lock = 0;
    temp_on_time_count = 0;
    temp_off_time = 0;

}

void deadlineHeuristicDisable()
{
    onHeuristicDisable();
    offHeuristicDisable();
    deadline_lock = 1;
}

void powerFailureHeuristicEnable()
{
    power_failure_lock = 0;
}

void powerFailureHeuristicDisable()
{
    power_failure_lock = 1;
}

const uint16_t _current_curve[DATA_POINTS_N] ={65535,2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,2000,1902,1793,1684,1575,1466,1357,1248,1139,1030,947,873,799,725,652,578,504,431,365,307,248,189,130,73,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

inline uint16_t fast_tardis(uint16_t _adc_12_value)
{
    int16_t left = _adc_12_value >> SHIFT_BITS;
    return  _current_curve[left] -
                ((
                    (uint32_t)(_current_curve[left] - _current_curve[left+1]) *
                    (uint32_t)(_adc_12_value - (left << SHIFT_BITS))
                ) >> SHIFT_BITS);
        };


/************* Off-time heuristic function *************/
void calcOffTime()
{
    int i = 0;
    switch (adapt)
    {
    case TASK_COUNT:
    {
        break;
    }
    case ON_TIME:
    {
        break;
    }
    default:
    {
#ifdef OFF_RECORD_FIRST
        if (!off_time_lock)
        {
            Timekeeper_config();
            for (i = HISTORY; i > 0; i--)
                offTime[i] = offTime[i - 1];
            //        offTime[0] = Timekeeper_sample();
            offTime[0] = (4095 - Timekeeper_sample());
            //        offTime[0] = fast_tardis(Timekeeper_sample());
            off_time_lock = 1;
        }
#elif defined(OFF_RECORD_LAST)
        if (!off_time_lock)
        {
            for (i = HISTORY; i > 0; i--)
                offTime[i] = offTime[i - 1];
            off_time_lock = 1;
        }
        Timekeeper_config();
        //    offTime[0] = Timekeeper_sample();
        offTime[0] = (4095 - Timekeeper_sample());
        //    offTime[0] = fast_tardis(Timekeeper_sample());
        offHeuristicEnable();
        off_time_lock = 1;
#else
        if (!off_time_lock)
        {
            for (i = HISTORY; i > 0; i--)
                offTime[i] = offTime[i - 1];
            offTime[0] = 0;
            off_time_lock = 1;
        }
        Timekeeper_config();
        //    offTime[0] = Timekeeper_sample();
//        temp_off_time += (4095 - Timekeeper_sample());
        temp_off_time += fast_tardis(Timekeeper_sample());
        offTime[0] = temp_off_time;
        offHeuristicEnable();
        off_time_lock = 1;
#endif
        break;
    }
    }
}

void setOffTime(uint16_t t)
{
    int i = 0;
    for (i = HISTORY; i >= 0; i--)
        offTime[i] = t;
}

uint16_t * getOffTime()
{
    temp_off_time = 0;
    return offTime;
}

/************* On-time heuristic function **************/
void calcOnTime()
{
    int i = 0;
    switch (adapt)
    {
    case TASK_COUNT:
    {
        break;
    }
    case OFF_TIME:
    {
        break;
    }
    default:
    {
#ifdef ON_RECORD_FIRST                                  // Good for computational applications
        if (!on_time_lock)
        {
            for (i = HISTORY; i > 0; i--)
                onTime[i] = onTime[i - 1];
            onTime[0] = temp_on_time_count;
            on_time_lock = 1;
        }
#elif defined(ON_RECORD_LAST)
        if (!on_time_lock)
        {
            for (i = HISTORY; i > 0; i--)
            onTime[i] = onTime[i - 1];
            on_time_lock = 1;
        }
        onTime[0] = 0xffff - temp_on_time_count;
//        onTime[0] = temp_on_time_count;
        temp_on_time_count = 0;
        onHeuristicEnable();
        on_time_lock = 1;
#else                                               // Good for applications with IOs (sleep mode)*/
        if (!on_time_lock)
        {
            for (i = HISTORY; i > 0; i--)
                onTime[i] = onTime[i - 1];
            onTime[0] = 0;
            on_time_lock = 1;
        }
//        onTime[0] = 0xffff - temp_on_time_count;
        onTime[0] += temp_on_time_count;
        onHeuristicEnable();
        on_time_lock = 1;
#endif
        break;
    }
    }
}

void setOnTime(uint16_t t)
{
    int i = 0;
    for (i = HISTORY; i >= 0; i--)
        onTime[i] = t;
    temp_on_time_count = t;
}

uint16_t * getOnTime()
{
    temp_on_time_count = 0;
    return onTime;
}

/************ Task count heuristic function ************/
void calcTaskCount()
{
    switch (adapt)
    {
    case TASK_COUNT:
    {
#ifdef TASK_RECORD_FIRST
        if (!task_count_lock)
        {
            int i = 0;
            for (i = HISTORY; i > 0; i--)
            taskCount[i] = taskCount[i - 1];
            taskCount[0] = temp_task_count;
            task_count_lock = 1;
        }
#elif defined(TASK_RECORD_LAST)
        if (!task_count_lock)
        {
            int i = 0;
            for (i = HISTORY; i > 0; i--)
            taskCount[i] = taskCount[i - 1];
            task_count_lock = 1;
        }
        taskCount[0] = temp_task_count;
        temp_task_count = 0;
#else
        if (!task_count_lock)
        {
            int i = 0;
            for (i = HISTORY; i > 0; i--)
                taskCount[i] = taskCount[i - 1];
            task_count_lock = 1;
        }
        taskCount[0] = temp_task_count;
#endif
        break;
    }
    default:
    {
        break;
    }
    }
}

void setTaskCount(uint32_t c)
{
    int i = 0;
    for (i = HISTORY; i >= 0; i--)
        taskCount[i] = c;
    temp_task_count = c;
}

void setTaskWeight(uint8_t w)
{
    task_weight = w;
}

uint32_t * getTaskCount()
{
    temp_task_count = 0;
    return taskCount;
}

void incTaskCount()
{
//    if (!task_count_lock)
    temp_task_count += task_weight;
}

/************** Deadline heuristic function **************/
void calcDeadlineTime()
{
    int i = 0;
    switch (adapt)
    {
    case DEADLINE:
    {
        if (!deadline_lock)
        {
            for (i = HISTORY; i > 0; i--)
                deadlineTime[i] = deadlineTime[i - 1];
            deadline_lock = 1;
        }
//        deadlineTime[0] = (getOnTime()[0] * 11.37);
//        deadlineTime[0] = getOffTime()[0];
        deadlineTime[0] = (getOnTime()[0] * 5.69) + getOffTime()[0];                // 120 / 10.55kHz

        deadlineHeuristicEnable();
        deadline_lock = 1;
        on_time_lock = 1;
        off_time_lock = 1;
        break;
    }
    default:
    {
        break;
    }
    }
}

void setDeadlineTime(uint32_t t)
{
    int i = 0;
    for (i = HISTORY; i >= 0; i--)
        deadlineTime[i] = t;
}

uint32_t * getDeadlineTime()
{
    return deadlineTime;
}


/************ Power failure count heuristic function ************/
void calcPowerFailureCount()
{
    switch (adapt)
    {
    case POWER_FAILURE_COUNT:
    {
        if (!power_failure_lock)
        {
            int i = 0;
            for (i = HISTORY; i > 0; i--)
                powerFailureCount[i] = powerFailureCount[i - 1];

            power_failure_lock = 1;
        }
        powerFailureCount[0] = temp_power_failure_count;
        break;
    }
    default:
    {
        break;
    }
    }
}

void incPowerFailureCount()
{
    temp_power_failure_count++;
}

void setPowerFailureCount(uint16_t c)
{
    int i = 0;
    for (i = HISTORY; i >= 0; i--)
        powerFailureCount[i] = c;
    temp_power_failure_count = c;
}

uint16_t * getPowerFailureCount()
{
    temp_power_failure_count = 0;
    return powerFailureCount;
}

void sleepMode(uint16_t cycles)
{
    TA2CCR0 = cycles;
    TA2CTL = TASSEL__ACLK | MC__UP | ID_3;     // SMCLK, UP mode
    TA2CCTL0 = CCIE;     // TACCR0 interrupt enabled
    __bis_SR_register(GIE);     // Enable general interrupt
    __bis_SR_register(LPM3_bits + GIE);     // LPM3, COMPE_ISR will force exit
    __no_operation();     // For debug only
}

// Timer0_A0 interrupt service routine  ---------- For on time recording
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer0_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) Timer0_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    if (!(P2IN & BIT5))
        temp_on_time_count++;
}

// Timer1_A0 interrupt service routine    ----------  For charging capacitor
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER1_A0_VECTOR
__interrupt void Timer1_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER1_A0_VECTOR))) Timer1_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    P2DIR |= BIT2;                              // making output
    P2OUT |= BIT2;                              // Charging pin on
    P2REN &= ~BIT2;                              // resistor disable
    __delay_cycles(100);
    P2DIR &= ~BIT2;                              // making input
    P2REN |= BIT2;                              // resistor enable
    P2OUT &= ~BIT2;                              // pull down

}

/// Timer2_A0 interrupt service routine    ----------  For sleep mode
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector = TIMER2_A0_VECTOR
__interrupt void Timer2_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER2_A0_VECTOR))) Timer2_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    TA2CTL = MC__STOP;
    TA2CCTL0 &= ~CCIE;                      // TACCR0 interrupt disabled
    __bic_SR_register_on_exit(LPM3_bits);                     // Exit active CPU
}
