// This file is part of InK.
// 
// author = "Kasım Sinan Yıldırım " 
// maintainer = "Kasım Sinan Yıldırım "
// email = "sinanyil81 [at] gmail.com" 
//  
// copyright = "Copyright 2018 Delft University of Technology" 
// license = "LGPL" 
// version = "3.0" 
// status = "Production"
//
// 
// InK is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

/*
 * This file has been modified for REHASH
 * Please visit InK repository to access the original code
 * https://github.com/TUDSSL/InK
 */

#include <libflicker/flicker.h>
#include <mcu/msp430/msp430fr59891.h>

void __clock_int()
{
    CSCTL0_H = CSKEY_H;                         // Unlock CS registers
    CSCTL1 = DCOFSEL_2 | DCORSEL;               // Set DCO to 8MHz
    CSCTL2 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
    CSCTL3 = DIVA__1 | DIVS__1 | DIVM__1;       // Set all dividers
    CSCTL0_H = 0;                               // Lock CS registers
}

void __mcu_init() {

    P1DIR = 0;                                  // 1- set p1 to input (0)
    P1REN = 0xFF;                               // 2- 0xff to enable pull up/down resistors for all 8 bits of p1
    P1OUT = 0;                                  // 3- pull down enable (0)

    P1SEL1 |= BIT3;                             // Configure P1.3 for ADC
    P1SEL0 |= BIT3;

    P2DIR = 0;
    P2REN = 0xFF;
    P2OUT = 0;

    P3DIR = 0;
    P3REN = 0xFF;
    P3OUT = 0;

    P4DIR = 0;
    P4REN = 0xFF;
    P4OUT = 0;

    P5DIR = 0;
    P5REN = 0xFF;
    P5OUT = 0;

    P6DIR = 0;
    P6REN = 0xFF;
    P6OUT = 0;

    P7DIR = 0;
    P7REN = 0xFF;
    P7OUT = 0;

    P8DIR = 0;
    P8REN = 0xFF;
    P8OUT = 0;

    P9DIR = 0;
    P9REN = 0xFF;
    P9OUT = 0;

    PJDIR = 0;
    PJREN = 0xFF;
    PJOUT = 0;

    WDTCTL = WDTPW | WDTHOLD;               // Stop watchdog timer

  // Disable FRAM wait cycles to allow clock operation over 8MHz
  FRCTL0 = 0xA500 | ((1) << 4); // FRCTLPW | NWAITS_1;
  __delay_cycles(3);

  /* init FRAM */
  FRCTL0_H |= (FWPW) >> 8;
  
  __delay_cycles(3);

//  __led_init(LED1);
//  __led_init(LED2);
  
  PM5CTL0 &= ~LOCKLPM5; // Disable the GPIO power-on default high-impedance mode
  __clock_int();

//  initPort2();
//  initPort3();
//
//  configPort2();
//  configPort3();

}
