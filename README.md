# REHASH

REHASH is a flexible heuristic-adaptation-based runtime for intermittently-powered devices. 

## How to Install REHASH
REHASH is provided as a compilable library. Import the kernel and application projects separately to Code Composer Studio and you should be able to compile, flash and run. In addition to the available applications, REHASH can be linked with any other project.

## Required hardware
Mostly tested on MSP430FR59891, but can work on any MCU of MSP430FR59 series.
