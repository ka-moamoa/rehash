// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <msp430.h>
#include <rehash.h>
#include <stdlib.h>
#include <math.h>
#include <include/mnist.h>
#include <include/input.h>

#define MIN_ADAPT_THRESH 0
#define MAX_ADAPT_THRESH 7
#define ADAPT_STEP_SIZE 1
//#define SKIP_CLASS

static __nv uint8_t adaptationLock = 0;
static __nv uint16_t app_Count = 0;

/******************* Application Variables *******************/
__knob(uint8_t, skipRows, MIN_ADAPT_THRESH);
__knob(uint8_t, skipColumns, MIN_ADAPT_THRESH);
__knob(uint8_t, skipClass, MIN_ADAPT_THRESH);

__ADAPT_UP
{
#ifdef SKIP_CLASS
    if (__READ_KNOB(skipClass) > MIN_ADAPT_THRESH)
    {
        __TUNE_UP(skipClass, ADAPT_STEP_SIZE);
    }
#else
    if (__READ_KNOB(skipRows) > MIN_ADAPT_THRESH)
    {
        __TUNE_UP(skipRows, ADAPT_STEP_SIZE);
        __TUNE_UP(skipColumns, ADAPT_STEP_SIZE);
    }
#endif
}

__ADAPT_DOWN
{
    __port_off(2, 4);
    __port_init(2, 4);
    __port_on(2, 4);
#ifdef SKIP_CLASS
    if (__READ_KNOB(skipClass) < MAX_ADAPT_THRESH)
    {
        __TUNE_DOWN(skipClass, ADAPT_STEP_SIZE);
    }
#else
    if (__READ_KNOB(skipRows) < MAX_ADAPT_THRESH)
    {
        __TUNE_DOWN(skipRows, ADAPT_STEP_SIZE);
        __TUNE_DOWN(skipColumns, ADAPT_STEP_SIZE);
    }
#endif
    __port_off(2, 4);
}

// Tasks.
ENTRY_TASK(task_init);
TASK(task_neural_network_hypothesis);
TASK(task_neural_network_softmax_max);
TASK(task_neural_network_softmax_sum);
TASK(task_neural_network_softmax);
TASK(task_classify);
TASK(task_done);
TASK(task_sleep);

__shared(int neuronNum, rowNum, prediction; float activations[MNIST_LABELS],
         max_activation; float softmaxMax, softmaxSum; int softmaxSumItr;)

// called at the very first boot
void thread1_init()
{
    // create a thread with priority 15 and entry task t_init
    __CREATE(15, task_init);
    __SIGNAL(15);
}

void __app_reboot()
{
    __port_init(9, 3);
    __port_on(9, 3);

#ifdef ADAPT
    signal_t adaptation_signal = ON_TIME;
    selectAdaptationHeuristic(adaptation_signal);
    if (!adaptationLock)
    {
        checkAdaptation();
        adaptationLock = 1;
    }
#endif

}

ENTRY_TASK(task_init)
{

    __SET(neuronNum, 0);
    __SET(rowNum, __READ_KNOB(skipRows));
    __SET(prediction, 0);
    __SET(max_activation, 0);
    __SET(softmaxMax, 0);
    __SET(softmaxSum, 0);
    __SET(softmaxSumItr, 0);

    __SET(activations[0], b[0]);

#ifdef ADAPT
    setTaskWeight(0);
#endif

    NEXT(task_neural_network_hypothesis);
}

//This code is implemented in task_neural_network_hypothesis
//for (i = 0; i < MNIST_LABELS; i++) {
//    activations[i] = b[i];
//    for (j = skip_rows; j < MNIST_IMAGE_HEIGHT-skip_rows; j++) {
//        for (k = skip_columns; k < MNIST_IMAGE_WIDTH-skip_columns; k++) {
//            activations[i] += W[i][j][k] * PIXEL_SCALE(image.pixels[j][k]);
//        }
//    }
//}

TASK(task_neural_network_hypothesis)
{
    int i = __GET(neuronNum);
    int j = __GET(rowNum);
    int k = __READ_KNOB(skipColumns);
    int skip_columns = __READ_KNOB(skipColumns);
    float tempActivations = __GET(activations[i]);

    for (; k < MNIST_IMAGE_WIDTH - skip_columns; k++)
    {

        tempActivations += W[i][j][k] * PIXEL_SCALE(number_pixels[j][k]);
    }

    __SET(activations[i], tempActivations);

#ifdef ADAPT
    setTaskWeight(21);
#endif

    if (j < (MNIST_IMAGE_HEIGHT - __READ_KNOB(skipRows)) - 1)
    {
        __SET(rowNum, ++j);
        NEXT(task_neural_network_hypothesis);
    }
    if (i < (MNIST_LABELS - __READ_KNOB(skipClass)) - 1)
    {
        __SET(neuronNum, ++i);
        __SET(rowNum, __READ_KNOB(skipRows));
        __SET(activations[i], b[i]);

        NEXT(task_neural_network_hypothesis);
    }
    else
        NEXT(task_neural_network_softmax_max);
}

TASK(task_neural_network_softmax_max)
{

    int i;
    float max;

    for (i = 1, max = __GET(activations[0]); i < (MNIST_LABELS - __READ_KNOB(skipClass));
            i++)
    {
        if (__GET(activations[i])> max)
        {
            max = __GET(activations[i]);
        }
    }

    __SET(softmaxMax, max);

#ifdef ADAPT
    setTaskWeight(1);
#endif

    NEXT(task_neural_network_softmax_sum);
}

TASK(task_neural_network_softmax_sum)
{
    int i = __GET(softmaxSumItr);
    float max = __GET(softmaxMax);
    float sum = __GET(softmaxSum);

    __SET(activations[i], exp(__GET(activations[i]) - max));
    sum += __GET(activations[i]);

    __SET(softmaxSum, sum);

#ifdef ADAPT
    setTaskWeight(28);
#endif

    if (i < (MNIST_LABELS - __READ_KNOB(skipClass)) - 1)
    {
        __SET(softmaxSumItr, ++i);
        NEXT(task_neural_network_softmax_sum);
    }
    else
        NEXT(task_neural_network_softmax);
}

TASK(task_neural_network_softmax)
{
    int i;
    float sum = __GET(softmaxSum);

    for (i = 0; i < (MNIST_LABELS - __READ_KNOB(skipClass)); i++)
    {
        __SET(activations[i], __GET(activations[i]) / sum);
    }

#ifdef ADAPT
    setTaskWeight(5);
#endif

    NEXT(task_classify);
}

TASK(task_classify)
{
    int predict = 0;
    int i = 0;

    __SET(max_activation, __GET(activations[0]));

    for (i = 0; i < (MNIST_LABELS - __READ_KNOB(skipClass)); i++)
    {
        if (__GET(max_activation) < __GET(activations[i]))
        {
            __SET(max_activation, __GET(activations[i]));
            predict = i;
        }
    }

    __SET(prediction, predict);

#ifdef ADAPT
    setTaskWeight(1);
#endif

    NEXT(task_done);
}
TASK(task_done)
{
    __port_off(4, 1);
    __port_init(4, 1);
    __port_on(4, 1);

    SpiDbgWrite(151);
    SpiDbgWrite(((app_Count + 1) >> 8) & 0xFF);
    SpiDbgWrite((app_Count + 1) & 0xFF);

    SpiDbgWrite(152);
    SpiDbgWrite(__READ_KNOB(skipRows));

    SpiDbgWrite(153);
    SpiDbgWrite(__READ_KNOB(skipClass));
    __port_off(4, 1);

    app_Count++;

#ifdef ADAPT
    setTaskWeight(1);
#endif

    NEXT(task_sleep);
}

TASK(task_sleep)
{
//    sleepMode(30000);

#ifdef ADAPT
    setTaskWeight(0);
#endif
    adaptationLock = 0;
//    checkAdaptation();

    NEXT(task_init);
}
