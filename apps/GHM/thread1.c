// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <msp430.h>
#include <rehash.h>
#include <include/cc1101.h>


#define MIN_ADAPT_THRESH 4
#define MAX_ADAPT_THRESH 128
#define ADAPT_STEP_SIZE 1

static __nv uint8_t adaptationLock = 0;
static __nv uint16_t app_Count = 0;
static __nv uint8_t radio_state = 0;
static __nv uint8_t tx_buffer[2] = { 0 };

/******************* Application Variables *******************/
typedef enum
{
    SEND_ALL = 1, SEND_AVG = 2, SEND_EVENT
} mode_t;

__knob(mode_t, mode, SEND_ALL);
__knob(uint8_t, num_samples, MAX_ADAPT_THRESH);

void adaptUp()
{
    if (__READ_KNOB(mode) == SEND_EVENT)
    {
        __WRITE_KNOB(mode, SEND_AVG);
        __WRITE_KNOB(num_samples, MAX_ADAPT_THRESH);
        return;
    }
    else if (__READ_KNOB(mode) == SEND_AVG)
    {
        __WRITE_KNOB(mode, SEND_ALL);
        __WRITE_KNOB(num_samples, MIN_ADAPT_THRESH);
        return;
    }
    if (((__READ_KNOB(num_samples) << ADAPT_STEP_SIZE) <= MAX_ADAPT_THRESH)  && (__READ_KNOB(mode) == SEND_ALL))
    {
        __WRITE_KNOB(num_samples, __READ_KNOB(num_samples) << ADAPT_STEP_SIZE);
    }
}

void adaptDown()
{
    __port_off(2, 4);
    __port_init(2, 4);
    __port_on(2, 4);

    if (__READ_KNOB(mode) == SEND_AVG)
    {
        __WRITE_KNOB(mode, SEND_EVENT);
        __WRITE_KNOB(num_samples, MAX_ADAPT_THRESH);
        return;
    }
    else if ((__READ_KNOB(num_samples) == MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_ALL))
    {
        __WRITE_KNOB(mode, SEND_AVG);
        __WRITE_KNOB(num_samples, MAX_ADAPT_THRESH);
        return;
    }
    if (((__READ_KNOB(num_samples) >> ADAPT_STEP_SIZE) >= MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_ALL))
    {
        __WRITE_KNOB(num_samples, __READ_KNOB(num_samples) >> ADAPT_STEP_SIZE);
    }
    __port_off(2, 4);
}

// Tasks.
ENTRY_TASK(task_init);
TASK(task_measure_temperature);
TASK(task_measure_moisture);
TASK(task_send_all);
TASK(task_compute_avg);
TASK(task_send_avg);
TASK(task_send_event);
TASK(task_done);
TASK(task_sleep);

__shared(
        uint16_t temperature[MAX_ADAPT_THRESH]; uint16_t avgTemperature; uint8_t temperatureSample;

        uint16_t moisture[MAX_ADAPT_THRESH]; uint16_t avgMoisture; uint8_t moistureSample;

        uint8_t samples_sent;)

// called at the very first boot
void thread1_init()
{
    // create a thread with priority 15 and entry task t_init
    __CREATE(15, task_init);
    __SIGNAL(15);
}

void __app_reboot()
{
    radio_state = 0;

    __port_init(9, 3);
    __port_on(9, 3);

#ifdef ADAPT
    signal_t adaptation_signal = POWER_FAILURE_COUNT;
    selectAdaptationHeuristic(adaptation_signal);
    if (!adaptationLock)
    {
        checkAdaptation();
        adaptationLock = 1;
    }
#endif

}

void radioInit()
{
    Init();                                 // Radio initialization
    __delay_cycles(1000);
    uint8_t flag = GetMARCState();
    SetDataRate(5);                         // Needs to be the same in Tx and Rx
    SetLogicalChannel(1);                   // Needs to be the same in Tx and Rx
    SetTxPower(7);
    Sleep();
}

void sendRadioPacket()
{
    onPort3();
    if (!radio_state)
    {
        radioInit();
        radio_state = 1;
    }
    Wakeup();
    __delay_cycles(100);
    SendDataNoWait(tx_buffer, 2);
    uint8_t flag = GetMARCState();
    Sleep();
    offPort3();
}

void voltageInterruptConfig()
{
    P3IES &= ~BIT3;             // trigger interrupt on a low to high transition
    P3IFG &= ~BIT3;                     // clear the interrupt flag just in case
    P3IE |= BIT3;                           // enable interrupt for P3.3

    __bis_SR_register(GIE);                 // Enable general interrupt
}

ENTRY_TASK(task_init)
{
    int i = 0;
    for (i = 0; i < MAX_ADAPT_THRESH; i++)
    {
        __SET(temperature[i], 0);
        __SET(moisture[i], 0);
    }
    __SET(avgTemperature, 0);
    __SET(avgMoisture, 0);
    __SET(temperatureSample, 0);
    __SET(moistureSample, 0);

#ifdef ADAPT
    setTaskWeight(1);
#endif

    NEXT(task_measure_temperature);
}

TASK(task_measure_temperature)
{
    unsigned sampleNum = __GET(temperatureSample);
    tempConfig();
    uint16_t temp = (uint16_t) tempDegC();
    __SET(temperature[sampleNum], temp);

    sampleNum++;
    __SET(temperatureSample, sampleNum);

#ifdef ADAPT
    setTaskWeight(2);
#endif

    if (sampleNum < __READ_KNOB(num_samples))
        NEXT(task_measure_temperature);
    NEXT(task_measure_moisture);
}


TASK(task_measure_moisture)
{
    unsigned sampleNum = __GET(moistureSample);
    onPort2();
    adcConfig();
    uint16_t moist = adcSample();
    offPort2();

    __SET(moisture[sampleNum], moist);

    sampleNum++;
    __SET(moistureSample, sampleNum);

#ifdef ADAPT
    setTaskWeight(0);
#endif

    if (sampleNum < __READ_KNOB(num_samples))
        NEXT(task_measure_moisture);

    switch (__READ_KNOB(mode))
    {
    case SEND_ALL:
        NEXT(task_send_all);
    default:
        break;
    }
    NEXT(task_compute_avg);
}

TASK(task_send_all)
{
    unsigned i = 0;
    i = __GET(samples_sent);

    tx_buffer[0] = __GET(temperature[i]);
    tx_buffer[1] = __GET(moisture[i]);

    P3DIR &= ~BIT3;                         // set to input pin
    P3REN |= BIT3;                          // resistor enable
    P3OUT &= ~BIT3;                         // make it pull down

    if (P3IN & BIT3)
        sendRadioPacket();
    else if (radio_state)
        sendRadioPacket();
    else
    {
        voltageInterruptConfig();
        __bis_SR_register(LPM3_bits);
        __no_operation();
    }

    __SET(samples_sent, i + 1);

#ifdef ADAPT
    setTaskWeight(27);
#endif

    if (__GET(samples_sent) < __READ_KNOB(num_samples))
        NEXT(task_send_all);
    else
        NEXT(task_done);
}

TASK(task_compute_avg)
{
    float temp = 0;
    float moist = 0;
    uint8_t i = 0;
    uint8_t tempCount = __GET(moistureSample);
    uint8_t wetCount = __GET(temperatureSample);

    for (i = 0; i < tempCount; i++)
        temp += __GET(temperature[i]);

    for (i = 0; i < wetCount; i++)
        moist += __GET(moisture[i]);

    temp /= tempCount;
    moist /= wetCount;

    __SET(avgTemperature, (uint16_t )temp);
    __SET(avgMoisture, (uint16_t )moist);

#ifdef ADAPT
    setTaskWeight(1 + (wetCount / 2));
#endif

    switch (__READ_KNOB(mode))
    {
    case SEND_AVG:
        NEXT(task_send_avg);
    case SEND_EVENT:
        NEXT(task_send_event);
    default:
        break;
    }
    NEXT(task_send_avg);
}

TASK(task_send_avg)
{
    tx_buffer[0] = (uint8_t) (__GET(avgTemperature));
    tx_buffer[1] = (uint8_t) (__GET(avgMoisture));

    P3DIR &= ~BIT3;                         // set to input pin
    P3REN |= BIT3;                          // resistor enable
    P3OUT &= ~BIT3;                         // make it pull down

    if (P3IN & BIT3)
        sendRadioPacket();
    else if (radio_state)
        sendRadioPacket();
    else
    {
        voltageInterruptConfig();
        __bis_SR_register(LPM3_bits);
        __no_operation();
    }

#ifdef ADAPT
    setTaskWeight(46);
#endif

    NEXT(task_done);
}

TASK(task_send_event)
{
#ifdef ADAPT
    setTaskWeight(0);
#endif

    if (__GET(avgTemperature) > 35)
    {
        tx_buffer[0] = (uint8_t) (__GET(avgTemperature));
        tx_buffer[1] = (uint8_t) (__GET(avgMoisture));

        P3DIR &= ~BIT3;                         // set to input pin
        P3REN |= BIT3;                          // resistor enable
        P3OUT &= ~BIT3;                         // make it pull down

        if (P3IN & BIT3)
            sendRadioPacket();
        else if (radio_state)
            sendRadioPacket();
        else
        {
            voltageInterruptConfig();
            __bis_SR_register(LPM3_bits);
            __no_operation();
        }

#ifdef ADAPT
        setTaskWeight(46);
#endif
    }
    NEXT(task_done);
}

TASK(task_done)
{
    __port_off(4, 1);
    __port_init(4, 1);
    __port_on(4, 1);

    SpiDbgWrite(151);
    SpiDbgWrite(((app_Count + 1) >> 8) & 0xFF);
    SpiDbgWrite((app_Count + 1) & 0xFF);

    SpiDbgWrite(152);
    SpiDbgWrite((uint8_t) __READ_KNOB(mode));

    SpiDbgWrite(153);
    SpiDbgWrite((uint8_t) __READ_KNOB(num_samples));
    __port_off(4, 1);

    app_Count++;

#ifdef ADAPT
    setTaskWeight(1);
#endif

    NEXT(task_sleep);
}

TASK(task_sleep)
{
    sleepMode(10);

#ifdef ADAPT
    setTaskWeight(0);
#endif
    adaptationLock = 0;
//    checkAdaptation();
    NEXT(task_init);
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT3_VECTOR
__interrupt void port3_isr_handler(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT3_VECTOR))) port3_isr_handler (void)
#else
#error Compiler not supported!
#endif
{
    __bic_SR_register(LPM3_bits);
    P3IFG &= ~BIT3;
    __bis_SR_register(GIE);

    sendRadioPacket();
    __bic_SR_register_on_exit(LPM3_bits);
}
