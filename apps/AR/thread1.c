// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <msp430.h>
#include <rehash.h>
#include <stdlib.h>
#include <include/accelerometer.h>
#include <include/ar.h>
#include <include/cc1101.h>

#define MIN_ADAPT_THRESH 4
#define MAX_ADAPT_THRESH 16
#define ADAPT_STEP_SIZE 1

static __nv uint8_t adaptationLock = 0;
static __nv uint16_t app_Count = 0;
static __nv uint8_t accelerometer_state = 0;
static __nv uint8_t radio_state = 0;
static __nv uint8_t tx_buffer[3] = { 0 };


/******************* Application Variables *******************/
typedef accelReading accelWindow[MAX_ADAPT_THRESH];

typedef enum
{
    SEND_ALL = 1, SEND_CLASS = 2, SEND_EVENT
} mode_t;

__knob(mode_t, mode, SEND_ALL);
__knob(uint8_t, num_samples, MAX_ADAPT_THRESH);


void adaptUp()
{
    if ((__READ_KNOB(num_samples) == MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_EVENT))
    {
        __WRITE_KNOB(mode, SEND_CLASS);
        return;
    }
    else if ((__READ_KNOB(num_samples) == MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_CLASS))
    {
        __WRITE_KNOB(mode, SEND_ALL);
    }
    if ((__READ_KNOB(num_samples) << ADAPT_STEP_SIZE) <= MAX_ADAPT_THRESH)
    {
        __WRITE_KNOB(num_samples, __READ_KNOB(num_samples) << ADAPT_STEP_SIZE);
    }
}

void adaptDown()
{
    __port_off(2, 4);
    __port_init(2, 4);
    __port_on(2, 4);

    if ((__READ_KNOB(num_samples) == MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_CLASS))
    {
        __WRITE_KNOB(mode, SEND_EVENT);
        return;
    }
    else if ((__READ_KNOB(num_samples) == MIN_ADAPT_THRESH) && (__READ_KNOB(mode) == SEND_ALL))
    {
        __WRITE_KNOB(mode, SEND_CLASS);
    }
    if ((__READ_KNOB(num_samples) >> ADAPT_STEP_SIZE) >= MIN_ADAPT_THRESH)
    {
        __WRITE_KNOB(num_samples, __READ_KNOB(num_samples) >> ADAPT_STEP_SIZE);
    }
}

// Tasks.
ENTRY_TASK(task_init);
TASK(task_acquire_window);
TASK(task_send_all);
TASK(task_transform_window);
TASK(task_featurize);
TASK(task_classify);
TASK(task_send_class);
TASK(task_send_event);
TASK(task_done);
TASK(task_sleep);

__shared(
        unsigned samples_in_window; uint8_t samples_sent; accelWindow window; features_t features; class_t prediction;)

// called at the very first boot
void thread1_init()
{
    // create a thread with priority 15 and entry task t_init
    __CREATE(15, task_init);
    __SIGNAL(15);
}

void __app_reboot(){
    accelerometer_state = 0;
    radio_state = 0;

    __port_init(9,3);
    __port_on(9, 3);

#ifdef ADAPT
    signal_t adaptation_signal =  ON_TIME;
    selectAdaptationHeuristic(adaptation_signal);
    if(!adaptationLock){
        checkAdaptation();
        adaptationLock = 1;
    }
#endif

}

void accel_sample(accelReading *sample)
{
    int x, y, z;
    initPort2();
    onPort2();
    accelerometerGetData8(&x, &y, &z);
    offPort2();
    sample->x = (long int) x;
    sample->y = (long int) y;
    sample->z = (long int) z;
}

void radioInit()
{
    Init();                                 // Radio initialization
    __delay_cycles(1000);
    uint8_t flag = GetMARCState();
    SetDataRate(5);                         // Needs to be the same in Tx and Rx
    SetLogicalChannel(1);                   // Needs to be the same in Tx and Rx
    SetTxPower(0);
    Sleep();
}

void sendRadioPacket()
{
    onPort3();
    if (!radio_state)
    {
        radioInit();
        radio_state = 1;
    }
    Wakeup();
    __delay_cycles(100);
    SendDataNoWait(tx_buffer, 3);
    uint8_t flag = GetMARCState();
    Sleep();
    offPort3();
}

void voltageInterruptConfig()
{
    P3IES &= ~BIT3;             // trigger interrupt on a low to high transition
    P3IFG &= ~BIT3;                     // clear the interrupt flag just in case
    P3IE |= BIT3;                           // enable interrupt for P3.3

    __bis_SR_register(GIE);                 // Enable general interrupt
}

ENTRY_TASK(task_init)
{
    __SET(samples_in_window, 0);
    __SET(samples_sent, 0);
    __SET(features.meanmag, 0);
    __SET(features.stddevmag, 0);
    __SET(prediction, CLASS_STATIONARY);
#ifdef ADAPT
    setTaskWeight(0);
#endif
    NEXT(task_acquire_window);
}

TASK(task_acquire_window)
{
    accelReading sample;
    unsigned sampleNum = __GET(samples_in_window);
    if (!accelerometer_state)
    {
        initPort2();
        onPort2();
        accelerometerInit();
        offPort2();
        accelerometer_state = 1;
    }

    accel_sample(&sample);
    __SET(window[sampleNum], sample);
    sampleNum++;
    __SET(samples_in_window, sampleNum);

#ifdef ADAPT
    setTaskWeight(18);
#endif
    if (sampleNum < __READ_KNOB(num_samples))
        NEXT(task_acquire_window);

    switch (__READ_KNOB(mode))
    {
    case SEND_ALL:
        NEXT(task_send_all);
    default:
        break;
    }
    NEXT(task_transform_window);
}

TASK(task_send_all)
{
    unsigned i = 0;

//    accelWindow aWin;
//    for (i = 0; i < num_samples; i++) {
//        aWin[i] = __GET(window[i]);
//    }

    i = __GET(samples_sent);
    tx_buffer[0] = (uint8_t) (__GET(window[i].x));
    tx_buffer[1] = (uint8_t) (__GET(window[i].y));
    tx_buffer[2] = (uint8_t) (__GET(window[i].z));

    P3DIR &= ~BIT3;                         // set to input pin
    P3REN |= BIT3;                          // resistor enable
    P3OUT &= ~BIT3;                         // make it pull down

    if (P3IN & BIT3)
        sendRadioPacket();
    else if (radio_state)
        sendRadioPacket();
    else
    {
        voltageInterruptConfig();
        __bis_SR_register(LPM3_bits);
        __no_operation();
    }

    __SET(samples_sent, i + 1);
#ifdef ADAPT
    setTaskWeight(30);
#endif

//    sleepMode(5000);

    if (__GET(samples_sent) < __READ_KNOB(num_samples))
        NEXT(task_send_all);
    else
        NEXT(task_done);
}

TASK(task_transform_window)
{
    unsigned i = 0;
    for (i = 0; i < __READ_KNOB(num_samples); i++)
    {
        accelReading sample = __GET(window[i]);

        if (sample.x < SAMPLE_NOISE_FLOOR || sample.y < SAMPLE_NOISE_FLOOR
                || sample.z < SAMPLE_NOISE_FLOOR)
        {

            sample.x = (sample.x > SAMPLE_NOISE_FLOOR) ? sample.x : 0;
            sample.y = (sample.y > SAMPLE_NOISE_FLOOR) ? sample.y : 0;
            sample.z = (sample.z > SAMPLE_NOISE_FLOOR) ? sample.z : 0;

            __SET(window[i], sample);
        }
    }

#ifdef ADAPT
    setTaskWeight(1 * __READ_KNOB(num_samples));
#endif
    NEXT(task_featurize);
}

TASK(task_featurize)
{
    accelReading mean;
    accelReading stddev;
    unsigned i;

    accelWindow aWin;
    for (i = 0; i < __READ_KNOB(num_samples); i++)
    {
        aWin[i] = __GET(window[i]);
    }

    mean.x = mean.y = mean.z = 0;
    stddev.x = stddev.y = stddev.z = 0;

    for (i = 0; i < __READ_KNOB(num_samples); i++)
    {
        mean.x += aWin[i].x;  // x
        mean.y += aWin[i].y;  // y
        mean.z += aWin[i].z;  // z
    }

    mean.x /= (float) __READ_KNOB(num_samples);
    mean.y /= (float) __READ_KNOB(num_samples);
    mean.z /= (float) __READ_KNOB(num_samples);

    for (i = 0; i < __READ_KNOB(num_samples); i++)
    {
        stddev.x +=
                aWin[i].x > mean.x ? aWin[i].x - mean.x : mean.x - aWin[i].x; // x
        stddev.y +=
                aWin[i].y > mean.y ? aWin[i].y - mean.y : mean.y - aWin[i].y; // y
        stddev.z +=
                aWin[i].z > mean.z ? aWin[i].z - mean.z : mean.z - aWin[i].z; // z
    }

    stddev.x /= (__READ_KNOB(num_samples) - 1);
    stddev.y /= (__READ_KNOB(num_samples) - 1);
    stddev.z /= (__READ_KNOB(num_samples) - 1);

    __SET(features.meanmag, (mean.x + mean.y + mean.z) / 3);
    __SET(features.stddevmag, (stddev.x + stddev.y + stddev.z) / 3);

#ifdef ADAPT
    setTaskWeight(10 + (2 * __READ_KNOB(num_samples)));
#endif
    NEXT(task_classify);
}
TASK(task_classify)
{
    int move_less_error = 0;
    int stat_less_error = 0;

    features_t features = __GET(features);
    features_t model_features;

    unsigned i;
    for (i = 0; i < MODEL_SIZE; ++i)
    {
        model_features = model.stationary[i];

        long int stat_mean_err =
                (model_features.meanmag > features.meanmag) ?
                        (model_features.meanmag - features.meanmag) :
                        (features.meanmag - model_features.meanmag);

        long int stat_sd_err =
                (model_features.stddevmag > features.stddevmag) ?
                        (model_features.stddevmag - features.stddevmag) :
                        (features.stddevmag - model_features.stddevmag);

        model_features = model.moving[i];

        long int move_mean_err =
                (model_features.meanmag > features.meanmag) ?
                        (model_features.meanmag - features.meanmag) :
                        (features.meanmag - model_features.meanmag);

        long int move_sd_err =
                (model_features.stddevmag > features.stddevmag) ?
                        (model_features.stddevmag - features.stddevmag) :
                        (features.stddevmag - model_features.stddevmag);

        if (move_mean_err < stat_mean_err)
        {
            move_less_error += move_mean_err;
        }
        else
        {
            stat_less_error += stat_mean_err;
        }

        if (move_sd_err < stat_sd_err)
        {
            move_less_error += move_sd_err;
        }
        else
        {
            stat_less_error += stat_sd_err;
        }
    }

    __SET(prediction,
          move_less_error > stat_less_error ? CLASS_MOVING : CLASS_STATIONARY);

#ifdef ADAPT
    setTaskWeight(10);
#endif

    switch (__READ_KNOB(mode))
    {
    case SEND_CLASS:
        NEXT(task_send_class);
    case SEND_EVENT:
        NEXT(task_send_event);
    default:
        break;
    }
    NEXT(task_send_class);
}
TASK(task_send_class)
{
    tx_buffer[0] = (uint8_t) (__GET(prediction));
    tx_buffer[1] = 0;
    tx_buffer[2] = 0;

    P3DIR &= ~BIT3;                         // set to input pin
    P3REN |= BIT3;                          // resistor enable
    P3OUT &= ~BIT3;                         // make it pull down

    if (P3IN & BIT3)
        sendRadioPacket();
    else if (radio_state)
        sendRadioPacket();
    else
    {
        voltageInterruptConfig();
        __bis_SR_register(LPM3_bits);
        __no_operation();
    }

#ifdef ADAPT
    setTaskWeight(33);
#endif

    NEXT(task_done);
}
TASK(task_send_event)
{
#ifdef ADAPT
    setTaskWeight(0);
#endif

    if (__GET(prediction) == CLASS_MOVING)
    {
        tx_buffer[0] = (uint8_t) (__GET(prediction));
        tx_buffer[1] = 0;
        tx_buffer[2] = 0;

        P3DIR &= ~BIT3;                         // set to input pin
        P3REN |= BIT3;                          // resistor enable
        P3OUT &= ~BIT3;                         // make it pull down

        if (P3IN & BIT3)
            sendRadioPacket();
        else if (radio_state)
            sendRadioPacket();
        else
        {
            voltageInterruptConfig();
            __bis_SR_register(LPM3_bits);
            __no_operation();
        }

#ifdef ADAPT
        setTaskWeight(33);
#endif
    }
    NEXT(task_done);
}
TASK(task_done)
{
    __port_off(4, 1);
    __port_init(4, 1);
    __port_on(4, 1);

    SpiDbgWrite(151);
    SpiDbgWrite(((app_Count + 1) >> 8) & 0xFF);
    SpiDbgWrite((app_Count + 1) & 0xFF);

    SpiDbgWrite(152);
    SpiDbgWrite((uint8_t) __READ_KNOB(mode));

    SpiDbgWrite(153);
    SpiDbgWrite((uint8_t) __READ_KNOB(num_samples));
    __port_off(4, 1);

    app_Count++;

#ifdef ADAPT
    setTaskWeight(2);
#endif

    NEXT(task_sleep);
}

TASK(task_sleep)
{
    sleepMode(10);

#ifdef ADAPT
    setTaskWeight(0);
#endif
    adaptationLock = 0;
//    checkAdaptation();
    NEXT(task_init);
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT3_VECTOR
__interrupt void port3_isr_handler(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(PORT3_VECTOR))) port3_isr_handler (void)
#else
#error Compiler not supported!
#endif
{
    __bic_SR_register(LPM3_bits);
    P3IFG &= ~BIT3;
    __bis_SR_register(GIE);

    sendRadioPacket();
    __bic_SR_register_on_exit(LPM3_bits);
}
