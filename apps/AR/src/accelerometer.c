// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <stdlib.h>
#include <include/accelerometer.h>
#include <rehash.h>

void accelerometerSpiInit(void)
{
    // initialize the SPI pins

    /* configure all SPI related pins */
    ACCELEROMETER_SPI_CONFIG_CSN_PIN_AS_OUTPUT();
    ACCELEROMETER_SPI_CONFIG_SCLK_PIN_AS_OUTPUT();
    ACCELEROMETER_SPI_CONFIG_SI_PIN_AS_OUTPUT();
    ACCELEROMETER_SPI_CONFIG_SO_PIN_AS_INPUT();

    /* set CSn to default high level */
    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();

    /* initialize the SPI registers */
    ACCELEROMETER_SPI_INIT();
}

uint8_t accelerometerSpiTransfer(uint8_t value)
{
    uint8_t statusByte;
    /* send the command strobe, wait for SPI access to complete */
    ACCELEROMETER_SPI_WRITE_BYTE(value);
    ACCELEROMETER_SPI_WAIT_DONE();

    /* read the radio status uint8_t returned by the command strobe */
    statusByte = ACCELEROMETER_SPI_READ_BYTE();
    //debug_stats[debug_index] = statusByte;
    //debug_index++;
    return statusByte;
}

void accelerometerSpiWriteReg(uint8_t addr, uint8_t value)
{
    ACCELEROMETER_SPI_DRIVE_CSN_LOW();
    accelerometerSpiTransfer(write_reg);
    accelerometerSpiTransfer(addr);
    accelerometerSpiTransfer(value);
    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();
}

uint8_t accelerometerSpiReadReg(uint8_t addr)
{
    uint8_t value;
    ACCELEROMETER_SPI_DRIVE_CSN_LOW();
    accelerometerSpiTransfer(read_reg);
    accelerometerSpiTransfer(addr);
    value = accelerometerSpiTransfer(nop);
    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();
    return value;
}


int accelerometerInit(void)
{
//    configPort2();

    accelerometerSpiInit();
    __delay_cycles(5000);

    if(accelerometerSpiReadReg(devid_ad) != 0xAD)            //test that default reg value can be read
        return -1;

    accelerometerSpiWriteReg(soft_reset, acc_reset);         //clear settings, sensor put in standby
    __delay_cycles(500);

    accelerometerSpiWriteReg(power_ctl, 0x02);
    __delay_cycles(500);

    return 0;
}

void accelerometerFifoInit(uint8_t len)
{
    accelerometerSpiWriteReg(fifo_samples, len);
    __delay_cycles(500);
    accelerometerSpiWriteReg(fifo_control, 0x01);                // 256 samples, oldest saved mode
    __delay_cycles(500);
    accelerometerSpiWriteReg(power_ctl, 0x02);
    __delay_cycles(500);
}

void accelerometerReadFifo(uint8_t len, accelReading **fifoData)
{
    int16_t x, y, z;
    accelReading *data = malloc(len * sizeof(accelReading));
    ACCELEROMETER_SPI_DRIVE_CSN_LOW();
    accelerometerSpiTransfer(read_fifo);
    unsigned i;
    for (i=0; i<len; i++){
        x = accelerometerSpiTransfer(nop);
        x |= (accelerometerSpiTransfer(nop) << 8);
        x = x << 4;
        x = x >> 4;
        data[i].x = (long int)x;

        y = accelerometerSpiTransfer(nop);
        y |= (accelerometerSpiTransfer(nop) << 8);
        y = y << 4;
        y = y >> 4;
        data[i].y = (long int)y;

        z = accelerometerSpiTransfer(nop);
        z |= (accelerometerSpiTransfer(nop) << 8);
        z = z << 4;
        z = z >> 4;
        data[i].z = (long int)z;

        fifoData[i] = &data[i];
    }
    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();
    free(data);
}

//Acceleration in g, 8 bit precision
void accelerometerGetData8(int *x, int *y, int *z)
{
    ACCELEROMETER_SPI_DRIVE_CSN_LOW();

    accelerometerSpiTransfer(read_reg);
    accelerometerSpiTransfer(xdata);

    //uint8_t cast to interpret as signed
    //Multiply by 16 to account for 4 bit shift from 12 bit value
    *x = (int8_t)accelerometerSpiTransfer(nop) * 16.0;
    *y = (int8_t)accelerometerSpiTransfer(nop) * 16.0;
    *z = (int8_t)accelerometerSpiTransfer(nop) * 16.0;

    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();
}

//Acceleration in g, 12 bit precision
void accelerometerGetData12(int *x, int *y, int *z)
{
    uint8_t xl,xh,yl,yh,zl,zh;

    ACCELEROMETER_SPI_DRIVE_CSN_LOW();

    accelerometerSpiTransfer(read_reg);
    accelerometerSpiTransfer(xdata_l);
    xl = accelerometerSpiTransfer(nop);
    xh = accelerometerSpiTransfer(nop);
    yl = accelerometerSpiTransfer(nop);
    yh = accelerometerSpiTransfer(nop);
    zl = accelerometerSpiTransfer(nop);
    zh = accelerometerSpiTransfer(nop);

    ACCELEROMETER_SPI_DRIVE_CSN_HIGH();

    *x = (((((int16_t)xh << 8) | (int16_t)xl) << 4) >> 4);
    *y = (((((int16_t)yh << 8) | (int16_t)yl) << 4) >> 4);
    *z = (((((int16_t)zh << 8) | (int16_t)zl) << 4) >> 4);
    //*t = (float)(((int16_t)zh << 8) | (int16_t)zl) * 0.065; //Sensitivity: 0.065 C / LSB
}
