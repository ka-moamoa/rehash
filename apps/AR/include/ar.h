// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AR_H_
#define AR_H_

#define MODEL_SIZE 16
#define SAMPLE_NOISE_FLOOR 10

typedef struct {
    long int meanmag;
    long int stddevmag;
} features_t;

typedef enum {
    CLASS_STATIONARY,
    CLASS_MOVING,
} class_t;

typedef struct {
    features_t stationary[MODEL_SIZE];
    features_t moving[MODEL_SIZE];
} model_t;

#pragma PERSISTENT(model)
model_t model = {{{346, 0}, {344, 2}, {342, 2}, {341, 0}, {346, 0}, {346, 0}, {342, 1}, {346, 0}, {346, 0}, {346, 0}, {345, 1}, {344, 2}, {342, 2}, {346, 0}, {346, 0}, {346, 0}},
                 {{868, 126}, {1441, 12}, {354, 48}, {607, 51}, {375, 83}, {655, 112}, {1270, 30}, {390, 32}, {504, 77}, {903, 208}, {678, 142}, {677, 0}, {911, 18}, {681, 205}, {677, 0}, {889, 32}}};


#endif /* AR_H_ */
