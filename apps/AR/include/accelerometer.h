// This file is part of REHASH evaluation.
//
// author = "Abu Bakar"
// maintainer = "Abu Bakar"
// email = "abubakar@u.northwestern.edu"
//
// copyright = "Copyright 2020 Northwestern University"
// license = "LGPL"
// version = "3.0"
// status = "Production"
//
//
// REHASH is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// REHASH is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef ACCELEROMETER_H_
#define ACCELEROMETER_H_


#include <stdint.h>
#include <msp430.h>
#include <stdint.h>

//Commands
#define nop         0x00
#define write_reg   0x0A
#define read_reg    0x0B
#define read_fifo   0x0D
#define acc_reset   0x52

//Read Registers
#define devid_ad        0x00    //0xAD
#define devid_mst       0x01    //0x1D
#define partid          0x02    //0xF2
#define revid           0x03
#define xdata           0x08    //8 MSB
#define ydata           0x09    //8 MSB
#define zdata           0x0A    //8 MSB
#define status          0x0B    //ERR_USER_REGS, AWAKE, INACT, ACT, FIFO_OVERRUN, FIFO_WATERMARK, FIFO_READY, DATA_READY
#define fifo_entries_l  0x0C    //Number of samples in FIFO buffer
#define fifo_entries_h  0x0D
#define xdata_l         0x0E    //8 LSB
#define xdata_h         0x0F    //Sign extended 4 MSB
#define ydata_l         0x10
#define ydata_h         0x11
#define zdata_l         0x12
#define zdata_h         0x13
#define temp_l          0x14
#define temp_h          0x15

//Read/Write Registers
#define soft_reset      0x1F    //Write 0x52 to reset accelerometer and put in standby
#define thresh_act_l    0x20    //8 LSB: Activity detected if 12 bit signed acceleration data > 11 bit unsigned THRESH_ACT
#define thresh_act_h    0x21    //3 MSB
#define time_act        0x22    //Number of consecutive samples that must have at least one axis > THRESH_ACT for activity to be detected
#define thresh_inact_l  0x23    //8 LSB: Inactivity detected if 12 bit signed acceleration data < 11 bit unsigned THRESH_INACT
#define thresh_inact_h  0x24    //3 MSB
#define time_inact_l    0x25    //Number of consecutive samples that must have at least one axis < THRESH_INACT for inactivity to be detected
#define time_inact_h    0x26    //8 MSB
#define act_inact_ctl   0x27    //x[7:6], LINK/LOOP[5:4], INACT_REF, INACT_EN, ACT_REF, ACT_EN
#define fifo_control    0x28    //x[7:4], AH, FIFO_TEMP, FIFO_MODE[1:0]
#define fifo_samples    0x29    //Number of samples to store in the FIFO (MSB is AH, range is 0 to 511)
#define filter_ctl      0x2C    //RANGE_1, RANGE_0, RES, HALF_BW, EXT_SAMPLE, ODR[2:0]
#define power_ctl       0x2D    //RES, EXT_CLK, LOW_NOISE[5:4], WAKEUP, AUTOSLEEP, MEASURE[1:0]


/* CSn Pin Configuration */
#define ACCELEROMETER_SPI_CSN_GPIO_BIT__             3
#define ACCELEROMETER_SPI_CONFIG_CSN_PIN_AS_OUTPUT()   st( P5DIR |=  BV(ACCELEROMETER_SPI_CSN_GPIO_BIT__); )
#define ACCELEROMETER_SPI_DRIVE_CSN_HIGH()             st( P5OUT |=  BV(ACCELEROMETER_SPI_CSN_GPIO_BIT__); ) /* atomic operation */
#define ACCELEROMETER_SPI_DRIVE_CSN_LOW()              st( P5OUT &= ~BV(ACCELEROMETER_SPI_CSN_GPIO_BIT__); ) /* atomic operation */
#define ACCELEROMETER_SPI_CSN_IS_HIGH()                 (  P5OUT &   BV(ACCELEROMETER_SPI_CSN_GPIO_BIT__) )

/* SCLK Pin Configuration */
#define ACCELEROMETER_SPI_SCLK_GPIO_BIT__            4
#define ACCELEROMETER_SPI_CONFIG_SCLK_PIN_AS_OUTPUT()  st( P1DIR |=  BV(ACCELEROMETER_SPI_SCLK_GPIO_BIT__); )
#define ACCELEROMETER_SPI_DRIVE_SCLK_HIGH()            st( P1OUT |=  BV(ACCELEROMETER_SPI_SCLK_GPIO_BIT__); )
#define ACCELEROMETER_SPI_DRIVE_SCLK_LOW()             st( P1OUT &= ~BV(ACCELEROMETER_SPI_SCLK_GPIO_BIT__); )

/* SI Pin Configuration */
#define ACCELEROMETER_SPI_SI_GPIO_BIT__              6
#define ACCELEROMETER_SPI_CONFIG_SI_PIN_AS_OUTPUT()    st( P1DIR |=  BV(ACCELEROMETER_SPI_SI_GPIO_BIT__); )
#define ACCELEROMETER_SPI_DRIVE_SI_HIGH()              st( P1OUT |=  BV(ACCELEROMETER_SPI_SI_GPIO_BIT__); )
#define ACCELEROMETER_SPI_DRIVE_SI_LOW()               st( P1OUT &= ~BV(ACCELEROMETER_SPI_SI_GPIO_BIT__); )

/* SO Pin Configuration */
#define ACCELEROMETER_SPI_SO_GPIO_BIT__              7
#define ACCELEROMETER_SPI_CONFIG_SO_PIN_AS_INPUT()     st( P1DIR &= ~BV(ACCELEROMETER_SPI_SO_GPIO_BIT__); \
                                                            P1REN |= BV(ACCELEROMETER_SPI_SO_GPIO_BIT__); \
                                                            P1OUT |= BV(ACCELEROMETER_SPI_SO_GPIO_BIT__);)

#define ACCELEROMETER_SPI_SO_IS_HIGH()                 ( P1IN & BV(ACCELEROMETER_SPI_SO_GPIO_BIT__) )

/* SPI Port Configuration */
#define ACCELEROMETER_SPI_CONFIG_PORT()                st( P1SEL1 &= ~(BV(ACCELEROMETER_SPI_SI_GPIO_BIT__) | BV(ACCELEROMETER_SPI_SO_GPIO_BIT__)| BV(ACCELEROMETER_SPI_SCLK_GPIO_BIT__)); \
                                                            P1SEL0 |= BV(ACCELEROMETER_SPI_SI_GPIO_BIT__) | BV(ACCELEROMETER_SPI_SO_GPIO_BIT__) | BV(ACCELEROMETER_SPI_SCLK_GPIO_BIT__);)
#define ACCELEROMETER_SPI_INIT() \
st ( \
  ACCELEROMETER_SPI_CONFIG_PORT();                       \
  UCB0CTLW0 |= UCSWRST;                 \
  UCB0CTLW0 |= UCCKPH | UCMSB | UCMST | UCSYNC;   \
  UCB0CTLW0 |= UCSSEL__SMCLK;                 \
  UCB0BRW  = 0x00;                                 \
  UCB0CTLW0 &= ~UCSWRST;                         \
)

/* read/write macros */
#define ACCELEROMETER_SPI_WRITE_BYTE(x)                st( UCB0IFG &= ~UCRXIFG;  UCB0TXBUF = x; )
#define ACCELEROMETER_SPI_READ_BYTE()                  UCB0RXBUF
#define ACCELEROMETER_SPI_WAIT_DONE()                  while(!(UCB0IFG & UCRXIFG));

typedef struct{
    long int x;
    long int y;
    long int z;
} threeAxis_t;

typedef threeAxis_t accelReading;

void accelerometerSpiInit();
uint8_t accelerometerSpiTransfer(uint8_t value);
void accelerometerSpiWriteReg(uint8_t addr, uint8_t value);
uint8_t accelerometerSpiReadReg(uint8_t addr);


int accelerometerInit(void);
void accelerometerGetData8(int *x, int *y, int *z);
void accelerometerGetData12(int *x, int *y, int *z);
void accelerometerFifoInit(uint8_t samples);
void accelerometerReadFifo(uint8_t len, accelReading **fifoData);

#endif /* ACCELEROMETER_H_ */
